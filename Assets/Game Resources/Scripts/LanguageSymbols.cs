﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Языковые символы
/// </summary>
public enum LanguageSymbols
{
    RU,
    EN
}
