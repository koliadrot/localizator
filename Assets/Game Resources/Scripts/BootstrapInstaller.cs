﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

/// <summary>
/// Инициилазтор связанностей
/// </summary>
public class BootstrapInstaller : MonoInstaller
{
    [SerializeField]
    private TranslateController translateController;

    /// <summary>
    /// Устанавливает связи
    /// </summary>
    public override void InstallBindings()
    {
        BindLocalizator();
    }

    private void BindLocalizator()
    {
        Container.Bind<TranslateController>().FromInstance(translateController).AsSingle();
    }
}
