﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

/// <summary>
/// Контроллер состояния локализации
/// </summary>
public class TranslateController : MonoBehaviour
{
    private List<IObservable> observables = new List<IObservable>();
    private LanguageSymbols languageSymbols;

    [SerializeField]
    private Dropdown dropdown;

    private void Start()
    {
        int enumCount = Enum.GetNames(typeof(LanguageSymbols)).Length;
        string currentLanguageSystem = System.Globalization.CultureInfo.CurrentCulture.ToString().Replace("-", " ").Split()[1];
        for (int i = 0; i < enumCount; i++)
        {
            languageSymbols = (LanguageSymbols)i;
            Dropdown.OptionData optionData = new Dropdown.OptionData();
            optionData.text = Enum.GetName(typeof(LanguageSymbols), languageSymbols).ToString();
            dropdown.options.Add(optionData);
        }
        dropdown.onValueChanged.AddListener(ctx => OnChangeLanguage());

        if (PlayerPrefs.HasKey("Localization"))
        {
            ChangeLanguage((LanguageSymbols)PlayerPrefs.GetInt("Localization"));
        }
        else
        {
            ChangeLanguage(LanguageSymbols.EN);
            for (int i = 0; i < enumCount; i++)
            {
                languageSymbols = (LanguageSymbols)i;
                if (Enum.GetName(typeof(LanguageSymbols), languageSymbols) == currentLanguageSystem)
                {
                    ChangeLanguage(languageSymbols);
                }
            }
        }
        dropdown.value = PlayerPrefs.GetInt("Localization") == 0 ? -1 : PlayerPrefs.GetInt("Localization");
    }
    /// <summary>
    /// Подписывает объект на наблюдателя
    /// </summary>
    /// <param name="observable"></param>
    public void AddObserver(IObservable observable)
    {
        observables.Add(observable);
    }
    /// <summary>
    /// Отписывает объект от наблюдателя
    /// </summary>
    /// <param name="observable"></param>
    public void RemoveObserver(IObservable observable)
    {
        observables.Remove(observable);
    }
    private void ChangeLanguage(LanguageSymbols symbol)
    {
        foreach (IObservable observer in observables)
        {
            observer.Notify(symbol);
        }
        PlayerPrefs.SetInt("Localization", ((int)symbol));
    }
    private void OnChangeLanguage()
    {
        languageSymbols = (LanguageSymbols)dropdown.value;
        ChangeLanguage(languageSymbols);
    }
}
