﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Интерфейс наблюдателя
/// </summary>
public interface IObservable
{
    /// <summary>
    /// Уведомление от наблюдателя
    /// </summary>
    /// <param name="symbol"></param>
    void Notify(LanguageSymbols symbol);
}

