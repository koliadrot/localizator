﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Zenject;

/// <summary>
/// Класс локализации конкретного объекта
/// </summary>
public class Localization : MonoBehaviour, IObservable
{
    private TranslateController translateController;

    private Text textLanguage;

    [SerializeField]
    private List<Languages> languages = new List<Languages>();

    [Serializable]
    private class Languages
    {
        [SerializeField]
        private string name;
        public LanguageSymbols typeLanguage;
        public string translation;
    }

    [Inject]
    private void Construct(TranslateController translateController)
    {
        this.translateController = translateController;
    }

    private void Awake()
    {
        TryGetComponent(out textLanguage);
    }

    private void OnEnable()
    {
        OnSubscribe();
    }

    private void OnDisable()
    {
        OnUnsubscribe();
    }

    private void OnSubscribe()
    {
        translateController.AddObserver(this);
    }

    private void OnUnsubscribe()
    {
        translateController.RemoveObserver(this);
    }

    void IObservable.Notify(LanguageSymbols symbol)
    {
        foreach (Languages language in languages)
        {
            if (language.typeLanguage == symbol && textLanguage != null)
            {
                textLanguage.text = language.translation;
            }
        }
    }
}
